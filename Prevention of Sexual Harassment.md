# # Prevention of Sexual Harassment

## 1. What kinds of behaviour cause sexual harassment?

Following are the kinds of behaviour that cause sexual harassment:

![This is image](https://www.workingnowandthen.com/wp-content/uploads/2021/11/08-Sexual-harassment-V03-700x384.png)

**Verbal**: Comments about clothing, A person's body, Sexual or gender based jokes or remarks, Requesting a sexual favour or asking a person out, sexual innuendo, Threads, spreading rumors about persons personal and sexual life, Foul and obscene language etc.

**Visual Harassment**: Visual harassment is caused by obscene poster drawings, picture screen savers, cartoons, emails, or texts of a sexual nature.

**Physical Harassment**: Sexual assort, impeding or blocking movement, inappropriate touching such as hugging, rubbing, kissing, etc., and sexual gesturing, leering, or staring are all examples of physical harassment.

![image](https://www.rappler.com/tachyon/r3-assets/612F469A6EA84F6BAE882D2B94A4B421/img/F5C8BD286F3E45AAB97883541B0E976C/sexual-harrassment-20160603.jpg)

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?

*Speak out against the harasser:* Talk with that person. Tell him that his actions are responsible for certain harassment, and clearly warn them that this will not be tolerated and that strict action will be taken against it.

*Report the harassment to the appropriate people:* If it still does not work, I will discuss this with my supervisor or manager and request that they take action on it.

 *Take legal action:* If it still doesn't work, I will take legal action, and for this I will get my guardians' help too.
 

![image](https://gumlet.assettype.com/nationalherald%2F2018-10%2F1991b45e-6a46-42a0-a7b6-42ade6b500d1%2FGRAPHIC.jpeg?rect=0%2C0%2C1400%2C788&w=1200&auto=format%2Ccompress&ogImage=true&enlarge=true)

