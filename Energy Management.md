# Energy Management

##  Manage Energy not Time

![image](https://gl-m.linker-cdn.net/article/2019/Oct/f4059a301bf5e75a8c35a388b19e047b.jpg)

### What are the activities you do that make you relax - Calm quadrant?

The activities that help me to relax and be calm are as follows:
* Excercise
* Playing indoor and outdoor games
* Bath in hot water
* Listening music
* Sleep
* Swimming
* Morning fresh air

### When do you find getting into the Stress quadrant?

* When completing tasks that are difficult to complete.
* During presentation, there is performance pressure.
* If the topic is not engaging while studying.
* If I'm not listening to music the entire day.
* when the submission deadline is closer.

### How do you understand if you are in the Excitement quadrant?

* Talking nonsense
* Laughing too much
* Spending money without any need
* After watching the motivational video
* After getting the unexpected gift.

## Sleep is your superpower

![image](https://i.ytimg.com/vi/gedoSfZvBgE/maxresdefault.jpg)

### Paraphrase the Sleep is your Superpower video in detail.

* Sleep is one of the most important factors influencing human health.
* In the learning aspect It is critical to take a nap before learning because it will act as a sponge to absorb the information, and without sleep, it will act like an inhibitor.
* When comparing well-sleepers to not-sleepers, well-sleepers found more benefit in learning new topics than not-sleepers.
* The hippocampus is in charge of data management in the human body; people who sleep well have a healthy hippocampus, whereas people who do not sleep have a damaged hippocampus.
* The shifting of a temporarily memorised concept into a permanent one will happen during sleep time.
* The most significant effect of sleep on health is ageing and demanture.
* Natural killer cells in the human body identify unwanted and dangerous viruses and work to eliminate them; lack of sleep has a negative impact on these cells.
* Sleep has a wide-ranging impact on the human body.
* There should be consistency in your sleep schedule, whether it is a weekday or any other day; your sleep and wake up times should be the same.
* Sleep is not an optional thing in a person's life; it is a non-negotiable biological necessity.

### What are some ideas that you can implement to sleep better?

* Keep mobile on silent.
* Turn on the fan.
* Use a warm blanket.
* Keep the lights off.

## Brain Changing Benefits of Exercise

![image](https://i0.wp.com/www.additudemag.com/wp-content/uploads/2008/01/exercise-and-the-adhd-brain-woman-biking-1920-GettyImages-491910554-e1649971309535.jpg)
### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

* Physical activity, which is simply moving your body, has immediate, long-term, and protective benefits for your brain that can last the rest of your life.
* The prefrontal cortex is part of the brain and is responsible for decision-making, focus, attention, and your personality. The temporal lobes are present on the left and right sides of the brain and are responsible for storing long-term memory in a structure called the hippocampus. The hippocampus is responsible for data processing in the human brain.
* Exercise or other body-moving activities make the brain stronger and keep it healthy.
* Better mood, energy, and attention are the aspects that come with exercise with respect to the brain.
* Exercise has an immediate effect on your mood by increasing levels of neurotransmitters such as dopamine, which leads to an increase in your mood after the exercise, as well as improving focus and reaction time.
* Exercise changes brain anatomy, physiology, and function.

### What are some steps you can take to exercise more?

* We will exercise in a set form.
* We will follow a weekly exercise pattern.
* We will drink a little water while exercising.
