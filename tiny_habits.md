# Tiny Habits

## Your takeaways from the video

![image](https://www.rajras.in/wp-content/uploads/2018/08/Behaviour-and-Intelligence.jpeg)
  
* The following are some small steps that we can take to celebrate our victories:  
	1. Make your hands up and just say "owsome."  
	2. Make your hands up and just say "bingo."  
	3. A Little dance  
* Practice changing the behavior, and it helps us know how the behaviour is working in our day-to-day lives.  
* People's behaviours are much simpler and less complicated than they believe.  
* There are a total of 15 ways in which humans can change their behaviour.  
* In order to achieve long-term change, we must first alter our environment and then change our behaviours through willpower.  
* To achieve success in any field, we must work on our behaviour rather than our goals.  
When you work on a tiny habit, you change your life and your behaviour.  
* Changes in human behaviour will be aided by some level of motivation, the ability to change behavior, and trigger actions.  
* Motivation will fade at some point, but because behaviour is a natural thing, it will emerge naturally.
 
 ## Your takeaways from the video in as much detail as possible.
![image](http://uwstartcenter.org/wp-content/uploads/2018/12/Fogg-Model-600x338.png)
 * Universal formula for behaviour is B=MAP 
 where ,
 B stand for Behaviour
 M stand for Motivation
 P stand for Prompt
* The above formula works on improving human behaviours with the help of tiny habits.
* The three areas that are useful for developing tiny habits are shrinking the behavior, identifying the action prompt, and growing your habit with the shine.

## How can you use B = MAP to make making new habits easier?

* Begin by focusing on small details.  
* Doing work that will require less motivation to accomplish the task  
* Increase the frequency of the tin work.

## Why it is important to "Shine" or Celebrate after each successful completion of habit?

* To enjoy the small task and to motivate ourselves.  
* It will instil confidence.

## Your takeaways from the video

* To ensure the success of small tasks, focus on the beginning and make it easy and simple.  
* A little improvement every day leads to achieving bigger goals in the long term.  
* Kill the habit: Consider what would happen if you didn't follow through on the habits in six months, then plan accordingly to avoid them.

## Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

![image](https://jamesclear.com/wp-content/uploads/2019/02/atomic-habits_gallery_hi-res_04.jpg)
  
* Atomic Habits is a book written by author James Clear that gives ideas about how tiny habits play an important role in changing life. 
* The book suggests many ways of adapting any habit and making that habit your style of living, With outcome-based habits, the focus is on what you want to achieve.
* It is beneficial to develop small habits in order to reduce stress and increase productivity.

## Write about the book's perspective on how to make a good habit easier?

Following are the ways by which we can make good habits easier:  
* Begin working early in the morning.  
* Change a bad habit into a good one.  
* Consistent meditation practice.
* Make a negative habit difficult to manage.
* Be inspired.

## Write about the book's perspective on making a bad habit more difficult?

Good habits are so important because they give us those gains in productivity on a daily basis, making them extra powerful in terms of compound interest, and bad habits are the inverse of that; hence, making it compounded by applying filters is to be suggested.

## Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

* Habit - Morning Walk
Steps - 
		1. Wake up early morning.
		2. Be motivated.
		3. Walk in nature, fresh air area.

## Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

* Habit: Overeating  
Steps -  
1. Inform your mother to serve less food.  
2. Inform your social roommate that you are on a diet.3. Inform everyone that you are no longer eating excessively.

