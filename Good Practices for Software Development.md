# Good Practices for Software Development

## What is your one major takeaway from each one of the 6 sections. So 6 points in total.

* Making notes while gathering requirements is essential.
* Things like project deadlines and issues that are required to be updated for team members related to work.
* Explain my issue so that a team member understands it.
* Join the meeting 5–10 minutes early.
* Be connected with team members via call, chat, etc.
* Participate in the project completely.


## Which area do you think you need to improve on? What are your ideas to make progress in that area?

* Asking questions in a meeting  
1. Keep in mind all the meeting discussion points.  
2. If you have any doubts, ask a question after the point is finished.
3. Making certain that I and others are on the same page.

![image](https://treinetic.com/wp-content/uploads/2021/07/Software-Development-Five-Best-Practices.jpg)

