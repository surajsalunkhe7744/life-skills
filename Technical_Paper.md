# FULL-TEXT SEARCH


### Problem Description

I joined a new project. The project is going through some performance issues. After some analysis, the team lead asks me to *investigate the possibility of using a different database for full-text searching* will improve performance.
So the investigation should be done on databases as follows - 
  1. Elasticsearch
  2. Solr
  3. Lucene
  
#### What is Full-Text Search?

Full-Text Search is a methodology for searching computer-stored document or a collection in a full-text database. Full-text search is distinguished from searches based on metadata or on parts of the original texts represented in databases (such as titles, abstracts, selected sections, or bibliographical references). 

#### What is Database?
![This is an image](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTL3vEUtndteUu-GTzVNeUzvTPc7xqzCZGQ4A&usqp=CAU)

Database is a place or medium where we store the data in systematic and organized manner. We use one of important operation is known as **CRUD**.



## Elasticsearch
![This is an image](https://upload.wikimedia.org/wikipedia/en/thumb/9/97/Elastic_NV_logo.svg/220px-Elastic_NV_logo.svg.png)

Elasticsearch is a distributed search and analytics engine built on Apache Lucene. Elasticsearch can be used to search any kind of document. It is based on powerful Lucence search engine. The original author(s) of elasticsearch is **Shay Babob** and it is developed by developer(s) of Elastic NV. It's initial release was released on **8 february 2010** and recently it's stable release was publised of version 7.17.7 and its released date was **25 October 2022**. It was written in java programming language.

#### Benefits
* The Elasticsearch supports following languages - 
arabic, armenian, basque, bengali, brazilian, bulgarian, catalan, cjk, czech, danish, dutch, english, estonian, finnish, french, galician, german, greek, hindi, hungarian, indonesian, irish, italian, latvian, lithuanian, norwegian, persian, portuguese, romanian, russian, sorani, spanish, swedish, turkish, thai etc.
* Elasticsearch provides functionality of Geo-Location search, it means that we can search the data based on its geographical.
* It is build for horizontal scalng.

#### Drawbacks
* Elasticsearch is free but not open source.
* Limitations include: architecture cannot be offered as a managed service and licensing cannot be removed or obscurd.

#### Some User's of Elasticsearch
 1. [LinkedIn](https://in.linkedin.com/)
 2. [Stack**overflow**](https://stackoverflow.com/)
 3. [Fujistu](https://www.fujitsu.com/global/)
 4. [Accenture](https://www.accenture.com/in-en?c=acn_glb_brandexpressiongoogle_12779745&n=psgs_0122&gclid=EAIaIQobChMI2byEp_rK-wIVApJmAh3qEQtZEAAYASAAEgJe2fD_BwE)
 5. [Tripwire](https://www.tripwire.com/)



## Solr
![This is an image](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQRWddbqDcHMDtsqoBPS_0cA--8E7ihSksvsqD1nfev4Lfv8Htnkxos-nB2UGqKn5QXTBs&usqp=CAU)

Apache solr is a open source search engine that is intended for enterprise text searches that can include information retrieval and analytics. Solr is developed by Apachi Software Foundation and it's stable release was published which is of 9.0.0 version and **12 May 2022** was it's release date. The solr is written in java programming language.

#### Benefits
The Solr supports following languages - 
arabic, brazilian portuguese, bulgarian, catalan, traditional chinese, CJK bigram filter, simplified chinese, HMM chinese tokenizer, czech, danish, dutch, finnish, french, galician, german, greek, hindi, indonesian, italian, irish, japanese, hebrew, lao, myanmar, khmer, latvian
, norwegian, persian, polish, portuguese, romanian, russian, scandinavian, serbian, spanish, swedish, thai, turkish, ukrainian etc.
Solr has large number of full-text search features.
Solr is purely open sourch where committers are based on merit.

#### Drawbacks
* The queries of Solr can be complex and Solr has limited functionality.
* Analytical capabilities of Solr is limited.

#### Some User's of Solr
 1. [Apple](https://www.apple.com/in/?afid=p238%7CsdUuvv563-dc_mtid_187079nc38483_pcrid_634106874218_pgrid_109516736379_pntwk_g_pchan__pexid__&cid=aos-IN-kwgo-brand--slid---product-)
 2. [AoL](https://www.aol.com/)
 3. [Cisco](https://www.cisco.com/c/en_in/index.html)
 4. [Netflix](https://www.netflix.com/in/)
 5. [Nasa](https://www.nasa.gov/)
 
 
 
## Lucene
![This is an image](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTipk3F53XCY7wrJfEyucCjlkb2AW5rIep5Ng&usqp=CAU)

Apache Lucene is a free and open-source search engine software library. Lucene is recognized for its utility in the implementation of Internet search engines and local, single-site searching. Lucene is develop by Apache Software Foundation and it's initial release was published in 1999. Recently it's stable release was publish of the version 9.4.2 and **November 21, 2022" was it's release date. The lucene is written in java programming language.

#### Benefits
* The Lucene supports following languages - 
chinese,russian,japanese,arabic,french,german etc.
* Lucence includes a feature to perform a fuzzy search based on edit distance.
* In terms of speed, there is none that can match Apache Lucene.
* In terms of Algorithm, it has ranked highest with best results returning first.
* Apache Lucene an open source program by Apache Software Foundation.

#### Drawback
Their are drawback from the business perspective. please refer following case for the same which is found on stackoverflow.

>I have limited experience with Lucene, so far it has been great though. The downsides I can see are mainly from a business perspective:
   I have to actively make the case for using Lucene to my boss, by default we would use SQL Server. To make the switch I will have to prove without a doubt that Lucene performs better> >(and not just similar) for the use case we have. I guess this one goes to the "Nobody ever got fired for buying IBM equipment" syndrome.
  Ongoing development/bug fixes for Lucene.Net in particular are questionable at this point, again a tougher sell w/o this. I hope the community can rally.

#### Some User's of Lucene
 1. [Dailymotion SA](https://www.dailymotion.com/us/topic/xc3i2)
 2. [Blackfriars Group](https://www.blackfriarsgroup.com/)
 3. [Red Hat Inc](https://www.redhat.com/en)
 4. [Therap Services LLC](https://www.therapservices.net/)


## Reference Links
* https://en.wikipedia.org/wiki/Full-text_search
* https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-lang-analyzer.html
* https://www.elastic.co/blog/geo-location-and-search
* https://lucidworks.com/post/who-uses-lucenesolr/
* https://logz.io/blog/15-tech-companies-chose-elk-stack/
* https://www.google.com/search?q=lucene+vs+solr&client=ubuntu&channel=fs&source=lnms&tbm=isch&sa=X&ved=2ahUKEwib_tjdp8n7AhUxzTgGHfLaAnsQ_AUoAXoECAEQAw&biw=1846&bih=968&dpr=1#imgrc=grOmcY-yeNXSpM
* https://en.wikipedia.org/wiki/Apache_Lucene
* https://stackoverflow.com/questions/10971261/languages-supported-by-lucene-net
* https://www.drupal.org/files/project-images/lucene_0.png
* https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Apache_Solr_logo.svg/1200px-Apache_Solr_logo.svg.png
* https://antaresnet.com/wp-content/uploads/2018/07/Elasticsearch-Logo-Color-V.png
