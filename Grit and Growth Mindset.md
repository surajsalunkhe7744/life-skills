# Grit and Growth Mindset

## 1. Grit

![image](https://www.grittv.com/wp-content/uploads/2014/08/Grit_Bluesky.png)

### Paraphrase (summarize) the video in a few lines. Use your own words.

* IQ is not the same for all people. Some strong people have a low IQ, and some weak people have a good one.
* Doing well in school and in life depends upon our ability to learn quickly and easily.
* Grit is perseverance and passion for long-term goals; grit is having stamina; grit is sticking with your future and working very hard to make that future a reality.
* Talent does not make us gritty. Grit is unrelated to talent.
* A growth mindset is a great idea for building grit.

### What are your key takeaways from the video to take action on?What are your key takeaways from the video to take action on?
* An increase in growth mindset will aid in gratitude.
* To be a saint, we must persevere for a long time; this may take years.
* Working hard is an important part of becoming gritty.
* Talent without grit is nothing, and grit is independent of talent.

## 2. Introduction to Growth Mindset

![image](https://www.nystromcounseling.com/wp-content/uploads/Fixed-vs.-Growth-mindset-2.jpg)

### Paraphrase (summarize) the video in a few lines in your own words.

* The growth mindset is the belief in one's own ability to learn and grow, and it is changing and improving the way people learn.
* People's mindsets play a crucial role in their success.
* People with a fixed mindset believe that you are not in control of your abilities; they think skills are born and you can't learn and grow, whereas people with a growth mindset believe the opposite.
* A growth mindset is the foundation for learning.
* Belief and focus are the two cornerstones of a growth mindset.
* The keys to growth are hard work, challenges, mistakes, and feedback. All these keys are byproducts of belief and focus.

### What are your key takeaways from the video to take action on?
* Focus and belief play an important role in a growth mindset, so it is important to stay focused, and belief in oneself is important.
* Growth midpoint has keys like hard work, challenges, mistakes, and feedback, which help to improve and succeed.
* A growth mindset provides a solid foundation for learning.

## 3. Understanding Internal Locus of Control

![image](https://cdn2.psychologytoday.com/assets/styles/manual_crop_1_91_1_1528x800/public/2019-06/shutterstock_1068480293.jpg?itok=uv2EIoWf)

### What is the Internal Locus of Control? What is the key point in the video?

* Locus of control is the degree to which you believe you have control over your life.
* External Locus of Control - Outside factors are outside because they have no control over them, but they can control them because they are intelligent enough to do so.
* Internal locus of control: Who worked hard and spent time on it, and they believe the outcome is the result of their hard work and extra efforts?How much you put into something is something that you have complete control over.
* To change ourself from an external locus of control to an internal locus of control, we have to start working on things to make them better, and that will give us self-belief.


## How to build a Growth Mindset

![image](https://thebeautyinbeinginsignificant.com/wp-content/uploads/2020/11/how-to-build-a-growth-mindset.png)
### Paraphrase (summarize) the video in a few lines in your own words.

* The first step toward a growth mindset is believing in your ability to figure things out.
* The second step toward a growth mindset is to question your assumptions. Don't let your assumptions limit or narrow your future vision.
* The third step toward a growth mindset is to develop your life curriculum. Don't wait for anything; instead, look into things that will assist you in obtaining that.
* The last step toward a growth mindset is to honour the struggle. Difficulties come with more struggle, and struggle makes you stronger. Difficulties are an important aspect of the growth mindset.


### What are your key takeaways from the video to take action on?

* Self-belief is the key to success, and it helps to be gritty.
* Don't let our assumptions overcome our vision, and try to stay positive and motivated.
* Set your curriculum toward your goals, be open to new things, and widen your mindset.
* Appreciate your struggle because it makes you better than your last one.

## Mindset - A MountBlue Warrior Reference Manual

![image](https://ekakshar.co.in/wp-content/uploads/2020/09/growth-mindset.png)

### What are one or more points that you want to take action on from the manual?

* I will give party to myself after I finish the work on problematic tasks.
* I will acquire the beleive in myself that it can be done by myself.






