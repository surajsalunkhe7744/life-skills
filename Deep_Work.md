#  Focus Management

## What is Deep Work?

![image](https://images.squarespace-cdn.com/content/v1/599d91c8f7e0abbe48d1f4a5/1570659796290-KJQ6LK9VHWQVZSUKXSGO/IMG_9169.PNG)

* Focusing without distraction on cognitively demanding task. Focus is a key for deep work.
* Deep work has more benefits than normal works. Deep implies intense concentration at work.
* Many people believe Focus is a tier 1 skill as deep work helps solve the problems efficiently.

## Paraphrase all the ideas in the above videos and this one in detail.

![image](https://myhc.church/images/uploads/message-graphics/Deep-Work-TITLE-SLIDE.jpg)

* Any deep work task will take more than 45 minutes or more to accomplish; it may differ from person to person as everyone has their own active focus mode.
* A deadline sets pressure for the accomplishment of a task, which will help the respective person stick with it and complete that task without any distraction or break during work time.
* It is good to have a deep work practise in our day-to-day lives. It will help to increase productivity and performance, which gives work satisfaction.
* Deep work should be done in the morning when you are in a good mood and have a clear mind.

## How can you implement the principles in your day to day life?

![image](https://renaissancemanhq.com/wp-content/uploads/2018/08/deepwork.png)

* Plan deep work for the morning.
* All distractions should be disabled while doing deep work.
* I will take a break after some periods, as I do regularly.
* I will verify that deep work is going on by checking productivity and requiring time.

## Your key takeaways from the video

![image](https://cdn.searchenginejournal.com/wp-content/uploads/2021/10/11-proven-tips-to-get-more-social-media-followers-1-61769bea5719c-sej.png)

* Social media is a two-edged sword: it brings people from long distances closer together, but as with any advantage, there is a disadvantage, much like the way social media affects today's people so strongly.
* Social media is the thing that consumes most of the active attention of humans and is a major distraction from our focus and deep work.
