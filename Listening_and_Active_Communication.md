#  Listening and Active Communication

### What are the steps/strategies to do Active Listening?

![image](https://images.yourstory.com/cs/1/1df24920ca7a11eaac607d1e03a5cbc9/activelistening-1595245062731.png)

Following are the things we have to keep in mind for active listening:
* Pay attention to the speaker and what he is saying.
* If someone is talking, don't interrupt them. Let them finish and then respond.
* By adding sentences, you show them that you are listening actively; those sentences are like, "Tell me more," "Go ahead; I am listening," "That sounds interesting," etc.
* Show them you are listening with your body language.
* Take notes to save important conversations for later use.
* Provide feedback on your conversation.

### According to Fisher's model, what are the key points of Reflective Listening?

![image](https://i.ytimg.com/vi/eUtZk960Q_A/maxresdefault.jpg)

Following is the summary for reflective listening:
* Reflective listening is a method for comprehending all of the stories told by the other person.
* This strategy is used to communicate at the nest level.
* Reflection Listening is used to deeply connect with the other person, and it helps to understand their situation.
* Listner should keep in mind the following points:  1) speaker content   2) the speaker's emotions   3) the speaker's meaning etc.


### What are the obstacles in your listening process?

![image](https://www.trainerslibrary.org/wp-content/uploads/2020/03/Are-you-really-listening-780x405.jpg)

The following are some of the barriers to listening:
* Distraction: Now days, major obstacles for listening are mobile phones, TV, etc.
* Preaccupation: Preparation entails thinking about other things while listening to others.
* Detailing: Too focused on the details.
* Topic: It seems difficult to focus on a specific topic if that topic is not of your interest.
* The Speaker: If the speaker is a soft talker or some kind of bore, then that may be the listening obstacle.


### What can you do to improve your listening?

Following are the points on which I can improve while listening to others:
* Everything that will distract you from listening will be turned off for a while.
* While listening, I will put myself in focused mode.
* I will make sure that all the points are clear and well understood.
* While listening, I will not interrupt the person who is talking; I will let them finish, then I will start talking.
* My body language will demonstrate that I am paying close attention to them.

### When do you switch to Passive communication style in your day to day life?

![image](https://learn.g2crowd.com/hubfs/passive%20communication%20style.png)

Following are the scenarios in which I will switch to passive communication:
* If a speaker is communicating information that does not relate to me.
* To avoid being overwhelmed by my mother's and father's and other family members words, I will practice passive listening.
* In workplace If there is a fault in my work, then in front of a senior, I practice passive listening.
* If I am unable to join my friends' party, then I have to practice passive listening.
* If someone is intentionally forcing me to do work that will affect me badly, I will practice aggressive communication.


### When do you switch into Aggressive communication styles in your day to day life?

![image](https://learn.g2crowd.com/hubfs/aggressive%20communication%20style.png)
* If someone is intentionally trying to harm me, my family, and my friends, then I will practice aggressive communication.
* If someone is doing bad things like disrespecting people or intentionally harming someone in a public place, then I will practice aggressive communication.
* If someone is intentionally breaking the rules in public places such as bus stops, airports, and so on, I will use aggressive communication.

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

![image](https://learn.g2crowd.com/hubfs/passive%20aggressive%20communication%20style.png)

* If my friends taunt others for their looks, hair, color, etc., then I will shift passive communication to aggressive communication.
* If my colleague makes false statements about our supervisor or manager in front of others, I will switch from passive to aggressive communication mode.
* If someone is scolding a person due to his physical disability, then I will switch my mode from passive to aggressive.


### How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life? (Watch the videos first before answering this question.)

![image](https://learn.g2crowd.com/hubfs/assertive%20communication%20style.png)

* If my request is ignored, then I will request them again, and for that I will use assertive communication.
* To practice assertive communication, I will prefer simple language.
* To practice assertive communication, I will verify that the other person understands my points and my situation very well.
* While practicing assertive communication, I will keep in mind that my words will not hurt them and they will get the point of why it is necessary.

![image](https://peritumagri.com/stride/pluginfile.php/724/mod_page/content/2/Picture1.png)
