# LEARNING PROCESS

## 1. How to Learn Faster with the Feynman Technique

![this is an image](https://www.brainscape.com/academy/content/images/2020/07/Richard-Feynman-technique-study.png)


### What is the Feynman Technique? Paraphrase the video in your own words.

Feynman Technique is used to learn anything by applying **4 rules** as follows -

* First pick and study the topic.
* Explain the topic to someone like a child, who is unfamiliar with the topic and with their level of understanding use simple language.
* Identify any gaps in your understanding.
* Return literature to understand the better.

If we apply those rules in our new learning process it helps us lot to learn new things and those rules we can apply in any learning process.


### What are the different ways to implement this technique in your learning process?

Currently, I am in the process of learning JavaScript, so this first rule of the Feynman Technique was applied here. After that, I will explain what I am learning to my friends or family members who are unfamiliar with the JavaScript, and I will try to deliver the ideas that I have gotten after studying the JavaScript. Their cross-questioning will help me understand where I want to work more, or which topics need more understanding, and after that I will work on that area too. So I can implement this by using this method in my learning process.


## 2. Learning How to Learn TED talk by Barbara Oakley

![this is an image](https://oaklandpostonline.com/wp-content/uploads/2021/07/profoakley-900x506.jpeg)


### Paraphrase the video in detail in your own words.

Barbara Oakley had given ideas about how our **brain works** and how we can do things which will gives high productivity. 

* Focus Mode and Defuse Mode: The human brain is in focus mode when humans are doing activities. Work may not occur during the focus mode if you are already overburdened with the tedious task. To overcome this, we need to put our minds in defuse mode and let that terrible task work its way into the brain as a background process. After some time, we can apply those background thoughts to our work. It will increase your productivity and your way of thinking about the issues.
* Procastination: Procastination is a situation in which you repeatedly neglect to do work. To avoid this, we must narrow our focus and concentrate solely on that one thing, focusing for 25 minutes before taking a 5-minute break to relax our minds. Relaxation is an important part of the learning process. By this means, our task will be completed easily.
* Illusion and Completion in Learning: If you are working on a problem, work on it as many times as possible; this will provide you with good practice on that topic and keep it in your mind for a long time. Understanding what is going on behind the scenes is critical. If you are reading books, don't just read and underline important topics; you can do some more reading after that, but try to recall what you have read; it will help you more than that.

### What are some of the steps that you can take to improve your learning process?

Currently, I am in the process of learning JavaScript. While learning JavaScript, I will keep in mind that I will later have to build the entire software with its assistance, so it is necessary to clear all of the fundamentals with practice, which will make JavaScript more familiar to me. Understanding plays a vital role in this, so understanding and practice will give me confidence and give sharpness to my work.

## 3. Learn Anything in 20 hours - Josh Kaufman

![this is an image](https://marriedline.com/wp-content/uploads/2019/06/josh-kaufma_198741.jpeg)

**"The major barrier to skill acquisition isn't intellectual. It's emotional."** - Josh Kaufma

### Your key takeaways from the video? Paraphrase your understanding.

New things are always comes with new learning.

* The 1000-Hour Rule: Practice helps more to learn something. You must invest 1000 hours to become an expert in something, which is a very long period, and we can't invest that much time in it while handling other work.
* The Learning Curve: When looking at this one, there are two scenarios to consider: if you learn more things without practicing them, it will take more time, and if you learn more things with more practice, it will take less time, so it is preferable to practice things while learning.
* Simple Steps Rapid Skill Acquisition: First of all, deconstruct the skill, i.e., break the skill into smaller pieces. Learn enough to do self-correction. Remove your practice barriers like TV, mobile, etc. Practice at least for 20 hours.

### What are some of the steps that you can while approaching a new topic?

While learning new things, I will break them down into pieces and then practically practice on them one by one, trying to understand them from all angles. All distractions will be turned off and full focus will be maintained while practicing.
